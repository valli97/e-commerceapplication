package com.shopping.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shopping.model.User;
import com.shopping.service.UserService;

@RestController
public class UserController {
	@Autowired
	UserService userService;

	@GetMapping("/users")
	public ResponseEntity<List<User>> getAll() {
		List<User> users = userService.getAll();
		return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}

	@PostMapping("")
	public ResponseEntity<String> addUser(@RequestBody User user) {
		userService.save(user);
		return new ResponseEntity<String>("User added successfully", HttpStatus.OK);
	}

	
}
