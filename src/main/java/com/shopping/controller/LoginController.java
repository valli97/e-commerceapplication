package com.shopping.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


import com.shopping.dto.UserDto;
import com.shopping.model.User;
import com.shopping.service.ConverterService;
import com.shopping.service.LoginService;

@RestController
public class LoginController {

	@Autowired
	private LoginService loginService;
	@Autowired
	ConverterService converterService;

	@PostMapping("/login")
	public UserDto productLogin(@RequestBody User user) {
	User checkLogin = loginService.userLogin(user);
		return converterService.convertToDto(checkLogin);
	}
	
}

 