package com.shopping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.shopping.model.Order;
import com.shopping.model.TransactionHistory;
import com.shopping.service.AddToCartProductServices;


@RestController
public class TransactionController {

	@Autowired
	private AddToCartProductServices buyProduct;

	@PostMapping("/course2")
	public ResponseEntity<String> SaveStudentByreParam(@RequestBody Order order) {
		return new ResponseEntity<String>(buyProduct.getStudentsByReqParam(order), HttpStatus.CREATED) ;
	}

}
