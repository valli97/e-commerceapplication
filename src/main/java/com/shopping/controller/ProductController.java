package com.shopping.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shopping.dto.ProductDto;
import com.shopping.exceptionalhandler.UserNotValidException;
import com.shopping.model.Product;
import com.shopping.service.ConverterService;
import com.shopping.service.ProductService;

@RestController
public class ProductController {
	@Autowired
	ProductService productService;
	@Autowired
	ConverterService converterService;
	
	
	@GetMapping(value = "/product")
	public ResponseEntity<List<Product>> productnameStartingWith(@RequestParam("productName") String productName) {
		List<Product> product = productService.findBynameStartingWith(productName);
		return new ResponseEntity<List<Product>>(product, HttpStatus.OK);
	}
}
	
	
