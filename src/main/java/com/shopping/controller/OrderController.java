package com.shopping.controller;

import java.util.Optional;

import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.shopping.model.Order;
import com.shopping.service.OrderService;
@RestController
public class OrderController {
	@Autowired
	OrderService orderService;
	
	@GetMapping("/order/{id}")
	public ResponseEntity<Object> getByOrderId(@PathVariable Integer id) {
		Optional<Order> order = null;
		try {
			order = orderService.findByOrderId(id);
		} catch (ResourceNotFoundException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Object>(order, HttpStatus.OK);
	}
}
