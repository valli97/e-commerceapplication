package com.shopping.exceptionalhandler;

public class ProductNotFoundException extends RuntimeException {
	
	public ProductNotFoundException(int productId) {
		super(String.format("Product with productId %d not found", productId));
	}

}
