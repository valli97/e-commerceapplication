package com.shopping.exceptionalhandler;

public class NoDataFoundException extends RuntimeException {
	
	public NoDataFoundException() {
		super("No data is found of your request");
	}

}
