package com.shopping.exceptionalhandler;

public class UserNotValidException extends RuntimeException {
	
	public UserNotValidException() {
		super("No data is found of your request");
	}

}
