package com.shopping.exceptionalhandler;

public class InvalidCredentialException extends RuntimeException {

	public InvalidCredentialException(String message) {
		super(message);

	}

}
