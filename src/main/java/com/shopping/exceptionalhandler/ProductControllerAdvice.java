package com.shopping.exceptionalhandler;
  
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
  
  @ControllerAdvice
  public class ProductControllerAdvice extends ResponseEntityExceptionHandler {
	  
	  @ExceptionHandler(ProductNotFoundException.class)
	    public ResponseEntity<Object> handleProductNotFoundException(ProductNotFoundException ex, WebRequest request) {

	        Map<String, Object> body = new LinkedHashMap<>();
	        body.put("timestamp", LocalDateTime.now());
	        body.put("message", "Prroduct of request type not found");
	        return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
	    }

	    @ExceptionHandler(NoDataFoundException.class)
	    public ResponseEntity<Object> handleNodataFoundException(NoDataFoundException ex, WebRequest request) {

	        Map<String, Object> body = new LinkedHashMap<>();
	        body.put("timestamp", LocalDateTime.now());
	        body.put("message", "No products found");
	        return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
	    }
	    
	    @ExceptionHandler(UserNotValidException.class)
	    public ResponseEntity<Object> handleUserNotValidException(UserNotValidException exception, WebRequest request){
	    	Map<String, Object> data = new LinkedHashMap<String, Object>();
	    	data.put("message", "not a valid user!!Enter correct details");
	    	return new ResponseEntity<Object>(data, HttpStatus.NOT_FOUND);
	    }
	    
	    @ExceptionHandler(NullPointerException.class)
	    public ResponseEntity<Object> handleNullPointerException(NullPointerException exception, WebRequest request){
	    	Map<String, Object> data = new LinkedHashMap<String, Object>();
	    	data.put("message", "not a valid user!!Enter correct details");
	    	return new ResponseEntity<Object>(data, HttpStatus.NOT_FOUND);
	    }
 
  }
 