package com.shopping.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shopping.model.User;

public interface UserRepository extends JpaRepository<User, String> {

	//public Optional<User> findByUserNameAndUserPassword(String userName,String userPassword);

	public Optional<User> findByUserNameAndUserPassword(String userName, String userPassword);

	
}
