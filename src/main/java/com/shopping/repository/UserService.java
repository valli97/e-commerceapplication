package com.shopping.repository;

import java.util.List;

import com.shopping.model.User;

public interface UserService {
public List<User>getAllUsers();
public User getUserById(long userId);
public List<User>getByUserName(String userName);

}

