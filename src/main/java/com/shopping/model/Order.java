package com.shopping.model;


import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
    @Entity
	@Table(name = "orders")
    @JsonIgnoreProperties ({"hibernateLazyInitializer","handler"})
	public class Order {
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		private int id;
        private String productName;
		private float amount;

		@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
		private Set<Product> productSet;
		
		@ManyToOne
		@JoinColumn(name = "user_id")
		private User user;

		
		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public float getAmount() {
			return amount;
		}

		public void setAmount(float amount) {
			this.amount = amount;
		}

		public Set<Product> getProductSet() {
			return productSet;
		}

		public void setProductSet(Set<Product> productSet) {
			this.productSet = productSet;
		}

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}

		
	}
