package com.shopping.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class TransactionHistory {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long transactionReferenceId;
	@Column
	private String fromSalryAccountNumber;
	@Column
	private String toBenificiaryAccountNumber;
	@Column
	private double amount;

	public Long getTransactionReferenceId() {
		return transactionReferenceId;
	}

	public void setTransactionReferenceId(Long transactionReferenceId) {
		this.transactionReferenceId = transactionReferenceId;
	}

	public String getFromSalryAccountNumber() {
		return fromSalryAccountNumber;
	}

	public void setFromSalryAccountNumber(String fromSalryAccountNumber) {
		this.fromSalryAccountNumber = fromSalryAccountNumber;
	}

	public String getToBenificiaryAccountNumber() {
		return toBenificiaryAccountNumber;
	}

	public void setToBenificiaryAccountNumber(String toBenificiaryAccountNumber) {
		this.toBenificiaryAccountNumber = toBenificiaryAccountNumber;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

}
