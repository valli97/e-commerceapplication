package com.shopping.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CeaditAccount {

	@Id
	private Long id;
	private double toAccount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getToAccount() {
		return toAccount;
	}

	public void setToAccount(double toAccount) {
		this.toAccount = toAccount;
	}

}
