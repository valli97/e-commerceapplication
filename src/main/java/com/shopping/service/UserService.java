package com.shopping.service;

import java.util.List;

import com.shopping.model.User;

public interface UserService {
	public List<User> getAll();
	public User save(User user);
	

}

