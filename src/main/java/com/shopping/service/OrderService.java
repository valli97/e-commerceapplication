package com.shopping.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopping.model.Order;
import com.shopping.repository.OrderRepository;

@Service
public class OrderService {
	@Autowired
	OrderRepository orderRepository;

	public Optional<Order> findByOrderId(Integer id) {
		return orderRepository.findById(id);

	}
	
	public Order saveOrder(Order  order) {
		return orderRepository.save(order);
		
	}

}
