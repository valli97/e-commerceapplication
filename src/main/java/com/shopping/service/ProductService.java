package com.shopping.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopping.exceptionalhandler.NoDataFoundException;
import com.shopping.model.Product;
import com.shopping.repository.ProductRepository;

@Service
public class ProductService {

	@Autowired
	ProductRepository productRepository;
	public List<Product> findBynameStartingWith(String name) {
		return productRepository.findByProductNameStartingWith(name);
	}


	

}
