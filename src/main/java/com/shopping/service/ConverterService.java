package com.shopping.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.shopping.dto.ProductDto;
import com.shopping.dto.UserDto;

import com.shopping.model.Product;
import com.shopping.model.User;

@Component
public class ConverterService {
	@Autowired
	private ModelMapper modelMapper;

	public ProductDto convertToDto(Product productObject) {
		return modelMapper.map(productObject, ProductDto.class);
	}
	
	public UserDto convertToDto(User userObject) {
		return modelMapper.map(userObject, UserDto.class);
	}
	
	
	}

