package com.shopping.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.shopping.exceptionalhandler.InvalidCredentialException;

import com.shopping.model.User;

import com.shopping.repository.UserRepository;

@Service
public class LoginService {

	@Autowired
	UserRepository userRepository;

	public User userLogin(User user) {

		Optional<User> loginUser = userRepository.findByUserNameAndUserPassword(user.getUserName(),
				user.getUserPassword());

		if (!loginUser.isPresent()) {
			throw new InvalidCredentialException(" check login details ");
		}

		User loginSuccessful = (User) loginUser.get();
		loginSuccessful.setLogin(true);
		userRepository.save(loginSuccessful);
		return loginSuccessful;

	}

	public User userLogout(User user) {

		Optional<User> loginUser = userRepository.findByUserNameAndUserPassword(user.getUserName(),
				user.getUserPassword());
		if (!loginUser.isPresent()) {
			throw new InvalidCredentialException(" check login details , Pls try again ");
		}
		User logoutSuccessful = (User) loginUser.get();
		logoutSuccessful.setLogin(false);
		userRepository.save(logoutSuccessful);
		return logoutSuccessful;

	}

	/*
	 * public boolean checkLoginValidation(String userName) {
	 * 
	 * Optional<User> loginUser = userRepository.findByUserName(userName); if
	 * (!loginUser.isPresent()) { throw new
	 * InvalidCredentialException(" check login details "); } User loginSuccessful =
	 * (User) loginUser.get(); if (loginSuccessful.isLogin() == false) { throw new
	 * InvalidCredentialException(" Not Logged "); } if (loginSuccessful.isLogin()
	 * == true) { return true; } return false; }
	 */

}