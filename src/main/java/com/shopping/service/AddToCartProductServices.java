package com.shopping.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.shopping.model.Order;
import com.shopping.model.TransactionHistory;
import com.shopping.repository.OrderRepository;

@Service
public class AddToCartProductServices {

	@Autowired
	RestTemplate restTemplate;
	@Autowired
	OrderRepository orderRepository;

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	public String getStudentsByReqParam(Order order) {

		order = orderRepository.save(order);
		Set<Double> d = order.getProductSet().stream().map(x -> x.getPrice()).collect(Collectors.toSet());
		double sum = d.stream().mapToDouble(Double::doubleValue).sum();
		int amount = (int) sum;
		String amount1 = Integer.toString(amount);

		String url = "http://localhost:9023/transfe";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		Map<String, String> params = new HashMap<String, String>();
		params.put("accountNumber", "2");
		params.put("beneficiaryAccountNumber", "12");
		params.put("amount", amount1);
		params.put("userId","95");

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
		for (Map.Entry<String, String> entry : params.entrySet()) {
			builder.queryParam(entry.getKey(), entry.getValue());
		}

		String result = restTemplate.getForObject(builder.toUriString(), String.class);
		return result;
	}

	/*
	 * public ResponseEntity<String> buyProductPostBody(TransactionHistory
	 * transactionHistory) { String uri = "http://localhost:9023/transfer";
	 * 
	 * HttpHeaders headers = new HttpHeaders();
	 * headers.setContentType(MediaType.APPLICATION_JSON);
	 * 
	 * JSONObject request = new JSONObject(); request.put("accountNumber",
	 * transactionHistory.getFromSalryAccountNumber()); request.put("beneficiary",
	 * transactionHistory.getToBenificiaryAccountNumber()); //
	 * request.put("referenceId", transactionHistory.getTransactionReferenceId());
	 * request.put("amount", transactionHistory.getAmount());
	 * 
	 * HttpEntity<String> entity = new HttpEntity<String>(request.toString(),
	 * headers); ResponseEntity<String> response = restTemplate.postForEntity(uri,
	 * entity, String.class); System.out.println(response); return response; }
	 */
}
