package com.shopping.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopping.model.User;
import com.shopping.repository.UserRepository;
@Service
@Transactional
public class UserServiceImpl implements UserService{
	@Autowired
	UserRepository userRepository;
	


	public List<User> getAll() {
		List<User> users = userRepository.findAll();
		return users;
	}

	public User save(User user) {
		return userRepository.save(user);
		
	}

	/*
	 * @Override public void deleteUserById(int userId) { // TODO Auto-generated
	 * method stub
	 * 
	 * }
	 */

	/*
	 * @Override public void save(User user) { // TODO Auto-generated method stub
	 * 
	 * }
	 * 
	 * @Override public void deleteUserById(int userId) { // TODO Auto-generated
	 * method stub
	 * 
	 * }
	 */
	/*
	 * public void deleteUserById(int userId) { userRepository.deleteById(userId);
	 * 
	 * }
	 */
}