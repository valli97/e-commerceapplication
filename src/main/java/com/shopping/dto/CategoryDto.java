package com.shopping.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CategoryDto {
	@JsonProperty
	private long categoryId;
	@JsonProperty
	private String categoryName;
	public long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
	
}
