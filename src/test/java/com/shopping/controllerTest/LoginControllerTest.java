package com.shopping.controllerTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


import com.shopping.controller.LoginController;
import com.shopping.model.User;
import com.shopping.repository.UserRepository;
import com.shopping.service.LoginService;

import junit.framework.Assert;


public class LoginControllerTest {
	@InjectMocks
	LoginController loginController;
	@Mock
	LoginService loginService;

	
	@Mock
	UserRepository userRepository;
	MockMvc mockMvc;
	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(loginController).build();
	}
	

	@Test
	public void testForValidateloginDetails() throws Exception {
		User login = new User(101, "amol", "admin");
		Mockito.when(userRepository.findByUserNameAndUserPassword(login.getUserName(), login.getUserPassword()))
		.thenReturn(Optional.of(login));
		MvcResult response = mockMvc.perform(MockMvcRequestBuilders.post("/login")).andReturn();
		String content = response.getResponse().getContentAsString();
		System.out.println(content);
		Assert.assertEquals("amol", login.getUserName());
		Assert.assertEquals("admin", login.getUserPassword());
	}
	

}
