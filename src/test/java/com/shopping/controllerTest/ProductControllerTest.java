package com.shopping.controllerTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner.Silent;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.shopping.controller.ProductController;
import com.shopping.model.Product;
import com.shopping.service.ProductService;
@RunWith(Silent.class)
public class ProductControllerTest {
	@InjectMocks
	ProductController productController;
	@Mock
	ProductService productService;

	MockMvc mockMvc;
	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(productController).build();
	}
	@Test
	public void testproductLoginMvc() throws Exception {
	      List<Product> p=new ArrayList();
		 Product product=new Product();
		 product.setProduct_Name("phone");
		 product.setPrice(500);
		 product.setProduct_Id(1);
		 p.add(product);
		 
		 Product product1=new Product();
		 product1.setProduct_Name("mobile");
		 product1.setPrice(300);
		 product1.setProduct_Id(1);
		 p.add(product1);
		
		Mockito.when(productService.findBynameStartingWith("phone")).thenReturn(p);

		MvcResult response = mockMvc.perform(MockMvcRequestBuilders.get("/product")).andReturn();
		String content = response.getResponse().getContentAsString();
		System.out.println(content);
	}

}
