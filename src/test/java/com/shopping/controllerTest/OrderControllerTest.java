package com.shopping.controllerTest;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner.Silent;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.shopping.controller.OrderController;
import com.shopping.model.Order;
import com.shopping.model.User;
import com.shopping.service.OrderService;


@RunWith(Silent.class)
public class OrderControllerTest {
	@InjectMocks
	OrderController orderController;
	@Mock
	OrderService orderService;

	MockMvc mockMvc;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(orderController).build();
	}
	
	@Test
	public void testgetByOrderIdMvc() throws Exception {
		Order order=new Order();
		order.setId(1);
		order.setAmount(250);
		User user=new User();
		user.setUserName("sai");
		user.setCity("nlr");
		user.setCountry("india");
		order.setUser(user);

		Mockito.when(orderService.findByOrderId(1)).thenReturn(Optional.of(order));

		MvcResult response = mockMvc.perform(MockMvcRequestBuilders.get("/order")).andReturn();
		String content = response.getResponse().getContentAsString();
		System.out.println(content);
	}
}
	
	
