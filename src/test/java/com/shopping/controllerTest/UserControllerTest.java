package com.shopping.controllerTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.shopping.controller.UserController;
import com.shopping.model.User;
import com.shopping.service.UserService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UserControllerTest {

	
	@InjectMocks
	UserController userController;
	
	@Mock
	UserService userService;
	
	@Test
	public void testGetAll() {
		List<User> users = new ArrayList<>();
		User user = new User();
		user.setUserId(100);
		user.setUserPassword("hsi");
		user.setLogin(false);
		users.add(user);
		
		user = new User();
		user.setUserId(101);
		user.setUserPassword("bye");
		user.setLogin(true);
		users.add(user);
		
		 Mockito.when(userService.getAll()).thenReturn(users);
		 List<User> users2 = userService.getAll();
			Assert.assertNotNull(users2);
			Assert.assertEquals(users, users2);
	}
	
}
