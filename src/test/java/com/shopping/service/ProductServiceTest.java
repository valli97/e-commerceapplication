package com.shopping.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.shopping.model.Product;
import com.shopping.repository.ProductRepository;
import com.shopping.repository.UserRepository;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ProductServiceTest {
	@InjectMocks
	ProductService productService;

	@Mock
	ProductRepository productRepository;

	@Test
	public void findBynameStartingWith() {
		 List<Product> p=new ArrayList();
		 Product product=new Product();
		 product.setProduct_Name("phone");
		 product.setPrice(500);
		 product.setProduct_Id(1);
		 p.add(product);

		Mockito.when(productRepository.findByProductNameStartingWith(product.getProductName()))
				.thenReturn(p);
		Assert.assertEquals("phone", product.getProductName());
	}

}
