package com.shopping.service;


import java.util.Optional;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner.Silent;

import com.shopping.model.Order;
import com.shopping.model.User;
import com.shopping.repository.OrderRepository;


@RunWith(Silent.class)
public class OrderServiceTest {
	
	@InjectMocks
	OrderService orderService;

	@Mock
	OrderRepository orderRepository;
	@Test
	public void findByOrderIdForPositive() {
		Order order=new Order();
		order.setId(1);
		order.setAmount(250);
		User user=new User();
		user.setUserName("sai");
		user.setCity("nlr");
		order.setUser(user);
	Mockito.when(orderRepository.findById(order.getId()))
		.thenReturn(Optional.of(order));
Assert.assertEquals(1, order.getId());
		
	}
	
	@Test
	public void findByOrderIdForNegativie() {
		Order order=new Order();
		order.setId(-1);
		order.setAmount(250);
		User user=new User();
		user.setUserName("sai");
		user.setCity("nlr");
		order.setUser(user);
	Mockito.when(orderRepository.findById(order.getId()))
		.thenReturn(Optional.of(order));
Assert.assertEquals(-1, order.getId());
		
	}
	
	
}