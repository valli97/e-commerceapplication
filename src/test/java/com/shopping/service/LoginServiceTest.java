package com.shopping.service;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.shopping.model.User;
import com.shopping.repository.UserRepository;

@RunWith(MockitoJUnitRunner.Silent.class)
public class LoginServiceTest {

	@InjectMocks
	LoginService loginCheckService;

	@Mock
	UserRepository userRepository;

	@Test
	public void testForValidateloginDetails() {
		User login = new User(101, "amol", "admin");
		Mockito.when(userRepository.findByUserNameAndUserPassword(login.getUserName(), login.getUserPassword()))
				.thenReturn(Optional.of(login));
		Assert.assertEquals("amol", login.getUserName());
		Assert.assertEquals("admin", login.getUserPassword());
	}

}
