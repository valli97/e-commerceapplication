package com.fundtransfer.api.exception;

public class DuplicateAccountIdException extends Exception {

	public DuplicateAccountIdException(String message) {
		super(message);

	}
}
