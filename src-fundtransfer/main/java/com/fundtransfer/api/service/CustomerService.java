package com.fundtransfer.api.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fundtransfer.api.dto.CustomerDto;
import com.fundtransfer.api.exception.InvalidCredentialException;
import com.fundtransfer.api.model.Account;
import com.fundtransfer.api.model.BeneficiaryAcoounts;
import com.fundtransfer.api.model.Customer;
import com.fundtransfer.api.repository.AccountRepository;
import com.fundtransfer.api.repository.BeneficiaryRepository;
import com.fundtransfer.api.repository.CustomerRepository;
import com.fundtransfer.api.utility.PasswordGeneration;

@Service
public class CustomerService {

	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private ConverterService converterService;

	PasswordGeneration password = password = new PasswordGeneration();

	public Customer saveCustomer(Customer customer) {

		String username = password.generateUsername(5);
		String pass = password.generatePassword(10);
		customer.setCustomerUserName(username);
		customer.setCustomerPassword(pass);
		return customerRepository.save(customer);

	}

	public List<CustomerDto> getAllCustomerList() {
		List<Customer> customerList = (List<Customer>) customerRepository.findAll();
		return customerList.stream().map(converterService::converToDto).collect(Collectors.toList());

	}

	public Customer valiateCustomerLoginDetails(String customerUsername, String customerPassword) {

		Customer checkLogin = customerRepository.findByCustomerUserNameAndCustomerPassword(customerUsername,
				customerPassword);

		if (customerUsername.equals(checkLogin.getCustomerUserName())
				&& customerPassword.equals(checkLogin.getCustomerPassword())) {

			return checkLogin;

		} else {
			throw new InvalidCredentialException(
					"Invalid CustomerLogin Details Pls try again ::" + customerUsername + " :: " + customerUsername);
		}

	}

	public Optional<Customer> findByCustomerId(Long customerId) {
		return customerRepository.findById(customerId);

	}

	public void deleteCustomer(Long customerId) {
		customerRepository.deleteById(customerId);

	}

	public void updateCustomer(Customer customer, Long customerId) {
		customerRepository.save(customer);
	}

	/*
	 * public CustomerDto saveCustomer(Customer customer) { Customer obj =
	 * customerRepository.save(customer); return converterService.convertToDto(obj);
	 * 
	 * }
	 */

}
