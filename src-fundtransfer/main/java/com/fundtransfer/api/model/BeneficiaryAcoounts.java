package com.fundtransfer.api.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class BeneficiaryAcoounts {

	@Id
	private String beneficiaryAcoountNumber;
	
	private double balance;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "accountNumber")
	private Account account;
	
	@JsonBackReference
	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public String getBeneficiaryAcoountNumber() {
		return beneficiaryAcoountNumber;
	}

	public void setBeneficiaryAcoountNumber(String beneficiaryAcoountNumber) {
		this.beneficiaryAcoountNumber = beneficiaryAcoountNumber;
	}



	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "BeneficiaryAcoounts [beneficiaryAcoountNumber=" + beneficiaryAcoountNumber + ", account=" + account
				+ "]";
	}

}
