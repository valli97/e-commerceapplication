package com.fundtransfer.api.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;

@Entity
public class Account {

	@Id
	@Column(name = "accountNumber", unique = true, nullable = false)
	private String accountNumber;
	@Column
	private String accountHolderName;
	@NotNull
	@Column
	private Double accountBalance;
	@Column
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date accountOpeningDate;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "customerId")
	private Customer customer;

	@OneToMany(mappedBy = "account", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<BeneficiaryAcoounts> beneficiaryAcoounts;

	public List<BeneficiaryAcoounts> getBeneficiaryAcoounts() {
		return beneficiaryAcoounts;
	}

	public void setBeneficiaryAcoounts(List<BeneficiaryAcoounts> beneficiaryAcoounts) {
		this.beneficiaryAcoounts = beneficiaryAcoounts;
	}

	public Account() {

	}

	public Account(String accountNumber) {
		this.accountNumber = accountNumber;

	}

	//@JsonBackReference
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getAccountHolderName() {
		return accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	public Date getAccountOpeningDate() {
		return accountOpeningDate;
	}

	public void setAccountOpeningDate(Date accountOpeningDate) {
		this.accountOpeningDate = accountOpeningDate;
	}

	public double getBalance() {
		return accountBalance;
	}

	public void setBalance(double balance) {
		this.accountBalance = balance;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

}
