package com.fundtransfer.api.controller;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fundtransfer.api.exception.DuplicateAccountIdException;
import com.fundtransfer.api.exception.ResourceNotFoundException;
import com.fundtransfer.api.model.Account;

import com.fundtransfer.api.model.Customer;
import com.fundtransfer.api.model.TransactionHistory;
import com.fundtransfer.api.service.AccountService;

@RestController
public class AccountController {

	@Autowired
	private AccountService accountService;

	@GetMapping("/accounts4")
	public ResponseEntity<List<Account>> getAllbeneficiaryAcoounts() {
		List<Account> accountList = accountService.getAllbeneficiaryAcoountsList();
		return new ResponseEntity<List<Account>>(accountList, HttpStatus.OK);
	}

	@PostMapping("/accounts1")
	public ResponseEntity<Object> saveAccount(@RequestBody Account account) throws DuplicateAccountIdException {
		accountService.saveAccountDetails(account);
		return new ResponseEntity<Object>(account.getAccountNumber(), HttpStatus.OK);
	}

	@PostMapping("/accounts")
	public ResponseEntity<Object> createAccount(@RequestBody Account account) {

		try {
			this.accountService.saveAccountDetails(account);
		} catch (DuplicateAccountIdException daie) {
			return new ResponseEntity<>(daie.getMessage(), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@GetMapping(path = "/accounts5/{accountNumber}")
	public Account getAccount(@PathVariable String accountNumber) {

		return this.accountService.getAccount(accountNumber);
	}

}
