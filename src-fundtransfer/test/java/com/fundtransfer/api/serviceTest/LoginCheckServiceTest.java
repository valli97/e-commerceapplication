package com.fundtransfer.api.serviceTest;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.fundtransfer.api.model.Customer;
import com.fundtransfer.api.model.Login;
import com.fundtransfer.api.repository.LoginRepository;
import com.fundtransfer.api.service.LoginCheckService;

import junit.framework.Assert;


@RunWith(MockitoJUnitRunner.Silent.class)
public class LoginCheckServiceTest {
	@InjectMocks
	LoginCheckService loginCheckService;
	@Mock
	LoginRepository loginRepository;
	@Test
	public void customerLogin() {
		Customer customer = new Customer();

		String loginUsername = customer.getCustomerUserName();
		String loginPassword = customer.getCustomerPassword();
		
		Optional<Login> userLogin = loginRepository.findByLoginUsernameAndLoginPassword(loginUsername, loginPassword);
		if(userLogin.isPresent()) {
			Login userLoginSuccess = (Login)userLogin.get();
			userLoginSuccess.setLogin(true);
			loginRepository.save(userLoginSuccess);
			Assert.assertNotNull(userLoginSuccess);
			Assert.assertEquals(userLogin,userLoginSuccess);
		}
	}
	
	
	@Test
	public void customerLogout() {
		Customer customer = new Customer();

		String loginUsername = customer.getCustomerUserName();
		String loginPassword = customer.getCustomerPassword();
		
		Optional<Login> userLogout = loginRepository.findByLoginUsernameAndLoginPassword(loginUsername, loginPassword);
		if(userLogout.isPresent()) {
			Login userLoginSuccess = (Login)userLogout.get();
			userLoginSuccess.setLogin(false);
			loginRepository.save(userLoginSuccess);
			Assert.assertNotNull(userLoginSuccess);
			Assert.assertEquals(userLogout,userLoginSuccess);
		}
	 }
	
	
	
	
	
	
	
	
	
	

}