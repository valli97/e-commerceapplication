package com.fundtransfer.api.serviceTest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fundtransfer.api.exception.DuplicateAccountIdException;
import com.fundtransfer.api.model.Account;
import com.fundtransfer.api.model.BeneficiaryAcoounts;
import com.fundtransfer.api.repository.AccountRepository;
import com.fundtransfer.api.service.AccountService;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)
public class AccountServiceTest {
	@InjectMocks
	AccountService accountService;
	@Mock
	AccountRepository accountRepository;

	@Test
	public void testgetAllbeneficiaryAcoountsList() {

		List<Account> a = new ArrayList();
		Account ac = new Account();
		ac.setAccountHolderName("sai");
		Date d = new GregorianCalendar(2004, 6, 9, 13, 45).getTime();
		ac.setBalance(1500);
		BeneficiaryAcoounts b = new BeneficiaryAcoounts();
		b.setBalance(1500);
		b.setBeneficiaryAcoountNumber("1234567");
		b.setAccount(ac);
		a.add(ac);
		Account ac1 = new Account();
		ac1.setAccountHolderName("jaya");
		ac1.setBalance(500);
		BeneficiaryAcoounts b1 = new BeneficiaryAcoounts();
		b1.setBalance(1500);
		b1.setBeneficiaryAcoountNumber("1234567");
		b1.setAccount(ac1);
		a.add(ac1);

		Mockito.when(accountRepository.findAll()).thenReturn(a);
		List<Account> acc = accountService.getAllbeneficiaryAcoountsList();
		Assert.assertNotNull(acc);
		Assert.assertEquals(2, acc.size());
	}
	/*@Test (expected =DuplicateAccountIdException.class )
	public void testsaveAccountDetails() throws Exception {
		Account ac = new Account();
		ac.setAccountHolderName("sai");
		Date d = new GregorianCalendar(2004, 6, 9, 13, 45).getTime();
		ac.setBalance(1500);
		BeneficiaryAcoounts b = new BeneficiaryAcoounts();
		b.setBalance(1500);
		b.setBeneficiaryAcoountNumber("1234567");
		b.setAccount(ac);
		Mockito.when(accountRepository.save(ac)).thenReturn(ac);
		Account acc = accountService.saveAccountDetails(ac);
		Assert.assertNotNull(acc);
		Assert.assertEquals(acc,ac);*/
	
	/* } */

	

}
