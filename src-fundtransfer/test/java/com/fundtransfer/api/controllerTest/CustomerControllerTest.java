package com.fundtransfer.api.controllerTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fundtransfer.api.controller.CustomerController;
import com.fundtransfer.api.dto.CustomerDto;
import com.fundtransfer.api.model.Customer;
import com.fundtransfer.api.service.CustomerService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class CustomerControllerTest {

	@InjectMocks
	CustomerController customerController;
	@Mock
	CustomerService customerService;

	MockMvc mockMvc;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(customerController).build();
	}

	@Test
	public void testgetAllCustomersMvc() throws Exception {
		List<CustomerDto> c=new ArrayList();
		CustomerDto customer = new CustomerDto();
		customer.setCustomerId((long) 1);
		customer.setCustomerUserName("mouni");
		c.add(customer);
        
        CustomerDto customer1 = new CustomerDto();
		customer1.setCustomerId((long) 1);
		customer1.setCustomerUserName("mouni");
		c.add(customer1);

		Mockito.when(customerService.getAllCustomerList()).thenReturn(c);

		MvcResult response = mockMvc.perform(MockMvcRequestBuilders.get("/customers")).andReturn();
		String content = response.getResponse().getContentAsString();
		System.out.println(content);
	}
	@Test
	public void testGetByIdMvc() throws Exception {
		Customer customer=new Customer();
		customer.setCustomerFirstName("sai");
		customer.setCustomerLastName("jaya");
		customer.setCustomerUserName("saijaya");
		customer.setCustomerCity("nellore");
		customer.setCustomerId((long) 123456789);
		Mockito.when(customerService.findByCustomerId((Mockito.anyLong()))).thenReturn(Optional.of(customer));

		MvcResult response = mockMvc.perform(MockMvcRequestBuilders.get("/customer")).andReturn();
		String content = response.getResponse().getContentAsString();
		System.out.println(content);
	}

	@Test
	public void testGetByIdMvcforneagitive() throws Exception {
		Customer customer=new Customer();
		customer.setCustomerFirstName("sai");
		customer.setCustomerLastName("jaya");
		customer.setCustomerUserName("saijaya");
		customer.setCustomerCity("nellore");
		customer.setCustomerId((long) 123456789);
		Mockito.when(customerService.findByCustomerId((Mockito.anyLong()))).thenReturn(Optional.of(customer));

		MvcResult response = mockMvc.perform(MockMvcRequestBuilders.get("/customer")).andReturn();
		String content = response.getResponse().getContentAsString();
		System.out.println(content);
	}

	@Test
	public void testsaveCustomerMvc() throws Exception {
		Customer customer=new Customer();
		customer.setCustomerFirstName("sai");
		customer.setCustomerLastName("jaya");
		customer.setCustomerUserName("saijaya");
		customer.setCustomerCity("nellore");
		customer.setCustomerId((long) 123456789);
		MvcResult response = mockMvc.perform(MockMvcRequestBuilders.post("/customer")).andReturn();
		String content = response.getResponse().getContentAsString();
		System.out.println(content);
	}

	@Test
	public void testUpdateMvc() throws Exception {
		Customer customer=new Customer();
		customer.setCustomerFirstName("sai");
		customer.setCustomerLastName("jaya");
		customer.setCustomerUserName("saijaya");
		customer.setCustomerCity("nellore");
		customer.setCustomerId((long) 123456789);

		MvcResult response = mockMvc.perform(MockMvcRequestBuilders.put("/customer")).andReturn();
		String content = response.getResponse().getContentAsString();
		System.out.println(content);
	}
	
}
