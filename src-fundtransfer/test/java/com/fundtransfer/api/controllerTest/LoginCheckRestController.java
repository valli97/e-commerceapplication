package com.fundtransfer.api.controllerTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.fundtransfer.api.model.Login;
import com.fundtransfer.api.service.LoginCheckService;

@RunWith(MockitoJUnitRunner.Silent.class)

public class LoginCheckRestController {

	@InjectMocks
	LoginCheckRestController loginCheckRestController;
	@Mock
	LoginCheckService loginCheckService;
	MockMvc mockMvc;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(loginCheckRestController).build();
	}

	@Test
	public void testLoginMvc() throws Exception {
		Login login = new Login();
		login.setLoginPassword("12344");
		login.setLoginUsername("sai");
		login.setLoginId(987);

		Mockito.when(loginCheckService.customerLogin(login)).thenReturn(null);

		MvcResult response = mockMvc.perform(MockMvcRequestBuilders.get("/login")).andReturn();
		String content = response.getResponse().getContentAsString();
		System.out.println(content);

	}
	@Test
	public void testLogoutMvc() throws Exception {
		Login login = new Login();
		login.setLoginPassword("12344");
		login.setLoginUsername("sai");
		login.setLoginId(987);

		Mockito.when(loginCheckService.customerLogin(login)).thenReturn(null);

		MvcResult response = mockMvc.perform(MockMvcRequestBuilders.get("/logou")).andReturn();
		String content = response.getResponse().getContentAsString();
		System.out.println(content);

	}


}
